import React from "react";
import "./Modal.css";

const Modal = ({ winner, logHref }) => {
  return (
    <div className="modal-layer">
      <div className="modal-root">
        <div className="modal-header">
          <span>Сongratulations, {winner.name}</span>
          <div onClick={() => window.location.reload()} className="close-btn">
            ×
          </div>
        </div>
        <div className="modal-body">
          <div className="modal-congratulations">
            Victory, victory,sweet victory... We knew that
            <span> {winner.name} </span>
            would win this fight, at least we believed in it !!!
            <span> {winner.name}</span> will be able to defeat anyone else
            (health left
            <span> {winner.health}</span>).
          </div>
          <div className="modal-wishes">...Shh, fight one more time</div>
          {logHref ? (
            <div className="modal-logHref">
              <a href={logHref} target="_blank">
                You can see the log of this game
              </a>
            </div>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
              style={{ margin: "auto", background: "#fff", display: "block" }}
              width="70px"
              height="70px"
              viewBox="0 0 100 100"
              preserveAspectRatio="xMidYMid"
            >
              <g transform="translate(20 50)">
                <circle cx="0" cy="0" r="6" fill="#5b7de1">
                  <animateTransform
                    attributeName="transform"
                    type="scale"
                    begin="-0.375s"
                    calcMode="spline"
                    keySplines="0.3 0 0.7 1;0.3 0 0.7 1"
                    values="0;1;0"
                    keyTimes="0;0.5;1"
                    dur="1s"
                    repeatCount="indefinite"
                  ></animateTransform>
                </circle>
              </g>
              <g transform="translate(40 50)">
                <circle cx="0" cy="0" r="6" fill="#5b7de1">
                  <animateTransform
                    attributeName="transform"
                    type="scale"
                    begin="-0.25s"
                    calcMode="spline"
                    keySplines="0.3 0 0.7 1;0.3 0 0.7 1"
                    values="0;1;0"
                    keyTimes="0;0.5;1"
                    dur="1s"
                    repeatCount="indefinite"
                  ></animateTransform>
                </circle>
              </g>
              <g transform="translate(60 50)">
                <circle cx="0" cy="0" r="6" fill="#5b7de1">
                  <animateTransform
                    attributeName="transform"
                    type="scale"
                    begin="-0.125s"
                    calcMode="spline"
                    keySplines="0.3 0 0.7 1;0.3 0 0.7 1"
                    values="0;1;0"
                    keyTimes="0;0.5;1"
                    dur="1s"
                    repeatCount="indefinite"
                  ></animateTransform>
                </circle>
              </g>
              <g transform="translate(80 50)">
                <circle cx="0" cy="0" r="6" fill="#5b7de1">
                  <animateTransform
                    attributeName="transform"
                    type="scale"
                    begin="0s"
                    calcMode="spline"
                    keySplines="0.3 0 0.7 1;0.3 0 0.7 1"
                    values="0;1;0"
                    keyTimes="0;0.5;1"
                    dur="1s"
                    repeatCount="indefinite"
                  ></animateTransform>
                </circle>
              </g>
            </svg>
          )}
        </div>
      </div>
    </div>
  );
};

export default Modal;
