import React from "react";
import "./arena.css";
import Modal from "./Modal/Modal.jsx";
import { controls } from "./controls";
import { createFight } from "../../services/domainRequest/fightRequest";

class Arena extends React.Component {
  state = {
    logs: [],
    winner: null,
    logHref: null,
  };

  componentDidMount() {
    const isPressedKeys = {
      PlayerOneAttack: 0,
      PlayerOneBlock: 0,
      PlayerTwoAttack: 0,
      PlayerTwoBlock: 0,
      PlayerOneCriticalHitCombination: { KeyQ: 0, KeyW: 0, KeyE: 0 },
      PlayerTwoCriticalHitCombination: { KeyU: 0, KeyI: 0, KeyO: 0 },
    };
    const firstFighter = this.props.fighter1;
    const secondFighter = this.props.fighter2;
    const healthFighters = [firstFighter.health, secondFighter.health];

    // set ability for criticalHit
    firstFighter.readyCriticalHit = true;
    secondFighter.readyCriticalHit = true;

    window.onkeydown = (e) => {
      onKeyDown(
        e,
        firstFighter,
        secondFighter,
        healthFighters,
        isPressedKeys,
        (winner) => {
          this.setWinner(winner);
        },
        (log) => {
          this.addLog(log);
        }
      );
    };
    window.onkeyup = (e) => onKeyUp(e, isPressedKeys);
  }

  async setWinner(winner) {
    this.setState({ winner });
    window.onkeydown = null;
    window.onkeyup = null;
    const logObj = await this.sendLog();
    const logHref = `/api/fights/${logObj.id}`;
    this.setState({ logHref });
  }

  addLog(log) {
    this.setState((state, props) => {
      return { logs: [...state.logs, log] };
    });
  }

  async sendLog() {
    const fight = {
      fighter1: this.props.fighter1.id,
      fighter2: this.props.fighter2.id,
      log: this.state.logs,
    };
    return createFight(fight);
  }

  render() {
    return (
      <div className="arenaRoot">
        <div className="arena___fight-status">
          <div className="arena___fighter-indicator">
            <span className="arena___fighter-name">
              {this.props.fighter1.name}
            </span>
            <div className="arena___health-indicator">
              <div
                className="arena___health-bar"
                id="left-fighter-indicator"
              ></div>
            </div>
          </div>
          <div className="arena___fighter-indicator">
            <span className="arena___fighter-name">
              {this.props.fighter2.name}
            </span>
            <div className="arena___health-indicator">
              <div
                className="arena___health-bar"
                id="right-fighter-indicator"
              ></div>
            </div>
          </div>
        </div>
        <div className="arena___battlefield">
          <div className="arena___fighter arena___left-fighter">
            <img
              src="https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif"
              className="fighter-preview___img"
              alt="fighter"
            />
          </div>
          <div className="arena___fighter arena___right-fighter">
            <img
              src="https://66.media.tumblr.com/tumblr_lq8g3548bC1qd0wh3o1_400.gif"
              className="fighter-preview___img"
              alt="fighter"
            />
          </div>
        </div>

        {this.state.winner && (
          <Modal winner={this.state.winner} logHref={this.state.logHref} />
        )}
      </div>
    );
  }
}

export default Arena;

function onKeyDown(
  e,
  firstFighter,
  secondFighter,
  healthFighters,
  isPressedKeys,
  resolve,
  addLog
) {
  // set pressed key and add state 'block' to fighter if needed
  switch (e.code) {
    case controls.PlayerOneAttack:
      isPressedKeys.PlayerOneAttack = 1;
      break;
    case controls.PlayerOneBlock:
      isPressedKeys.PlayerOneBlock = 1;
      document
        .querySelector(".arena___fighter.arena___left-fighter")
        .classList.add("block");
      break;
    case controls.PlayerTwoAttack:
      isPressedKeys.PlayerTwoAttack = 1;
      break;
    case controls.PlayerTwoBlock:
      isPressedKeys.PlayerTwoBlock = 1;
      document
        .querySelector(".arena___fighter.arena___right-fighter")
        .classList.add("block");
      break;
    case controls.PlayerOneCriticalHitCombination[0]:
      isPressedKeys.PlayerOneCriticalHitCombination[
        controls.PlayerOneCriticalHitCombination[0]
      ] = 1;
      break;
    case controls.PlayerOneCriticalHitCombination[1]:
      isPressedKeys.PlayerOneCriticalHitCombination[
        controls.PlayerOneCriticalHitCombination[1]
      ] = 1;
      break;
    case controls.PlayerOneCriticalHitCombination[2]:
      isPressedKeys.PlayerOneCriticalHitCombination[
        controls.PlayerOneCriticalHitCombination[2]
      ] = 1;
      break;
    case controls.PlayerTwoCriticalHitCombination[0]:
      isPressedKeys.PlayerTwoCriticalHitCombination[
        controls.PlayerTwoCriticalHitCombination[0]
      ] = 1;
      break;
    case controls.PlayerTwoCriticalHitCombination[1]:
      isPressedKeys.PlayerTwoCriticalHitCombination[
        controls.PlayerTwoCriticalHitCombination[1]
      ] = 1;
      break;
    case controls.PlayerTwoCriticalHitCombination[2]:
      isPressedKeys.PlayerTwoCriticalHitCombination[
        controls.PlayerTwoCriticalHitCombination[2]
      ] = 1;
      break;
    default:
      break;
  }

  // make one hit
  makeHit(isPressedKeys, firstFighter, secondFighter, healthFighters, addLog);

  // checking for a winner
  if (firstFighter.health === 0 || secondFighter.health === 0) {
    resolve(firstFighter.health === 0 ? secondFighter : firstFighter);
  }
}

function makeHit(
  isPressedKeys,
  firstFighter,
  secondFighter,
  healthFighters,
  addLog
) {
  const [healthFirstFighter, healthSecondFighter] = healthFighters;

  const isPlayerOneCriticalHitCombination =
    isPressedKeys.PlayerOneCriticalHitCombination.KeyQ &&
    isPressedKeys.PlayerOneCriticalHitCombination.KeyW &&
    isPressedKeys.PlayerOneCriticalHitCombination.KeyE;
  const isPlayerTwoCriticalHitCombination =
    isPressedKeys.PlayerTwoCriticalHitCombination.KeyU &&
    isPressedKeys.PlayerTwoCriticalHitCombination.KeyI &&
    isPressedKeys.PlayerTwoCriticalHitCombination.KeyO;

  // Can firstFighter make the hit
  if (
    ((isPressedKeys.PlayerOneAttack && !isPressedKeys.PlayerTwoBlock) ||
      (isPlayerOneCriticalHitCombination && firstFighter.readyCriticalHit)) &&
    !isPressedKeys.PlayerOneBlock
  ) {
    let damage;
    if (isPlayerOneCriticalHitCombination && firstFighter.readyCriticalHit) {
      firstFighter.readyCriticalHit = false;
      firstFighter.isCritical = true;
      damage = getDamage(firstFighter, secondFighter);
      setTimeout(() => (firstFighter.readyCriticalHit = true), 10000);
    } else {
      damage = getDamage(firstFighter, secondFighter);
    }

    animateHit(
      ".arena___fighter.arena___left-fighter",
      ".arena___fighter.arena___right-fighter"
    );

    secondFighter.health -= damage;
    secondFighter.health = secondFighter.health < 0 ? 0 : secondFighter.health;

    addLog({
      fighter1Shot: damage,
      fighter2Shot: 0,
      fighter1Health: firstFighter.health,
      fighter2Health: secondFighter.health,
    });

    const rightHealthIndicator = document.querySelector(
      "#right-fighter-indicator"
    );
    rightHealthIndicator.style.width = `${
      (secondFighter.health / healthSecondFighter) * 100
    }%`;
  }

  // Can secondFighter make the hit
  if (
    ((isPressedKeys.PlayerTwoAttack && !isPressedKeys.PlayerOneBlock) ||
      (isPlayerTwoCriticalHitCombination && secondFighter.readyCriticalHit)) &&
    !isPressedKeys.PlayerTwoBlock
  ) {
    let damage;

    if (isPlayerTwoCriticalHitCombination && secondFighter.readyCriticalHit) {
      secondFighter.readyCriticalHit = false;
      secondFighter.isCritical = true;
      damage = getDamage(secondFighter, firstFighter);
      setTimeout(() => (secondFighter.readyCriticalHit = true), 10000);
    } else {
      damage = getDamage(secondFighter, firstFighter);
    }

    animateHit(
      ".arena___fighter.arena___right-fighter",
      ".arena___fighter.arena___left-fighter"
    );

    firstFighter.health -= damage;
    firstFighter.health = firstFighter.health < 0 ? 0 : firstFighter.health;

    addLog({
      fighter1Shot: 0,
      fighter2Shot: damage,
      fighter1Health: firstFighter.health,
      fighter2Health: secondFighter.health,
    });

    const leftHealthIndicator = document.querySelector(
      "#left-fighter-indicator"
    );
    leftHealthIndicator.style.width = `${
      (firstFighter.health / healthFirstFighter) * 100
    }%`;
  }
}

function animateHit(attackerSelector, defenderSelector) {
  const attacker = document.querySelector(attackerSelector);
  const defender = document.querySelector(defenderSelector);
  attacker.classList.add("hitting");
  defender.classList.add("defending");
  setTimeout(() => {
    attacker.classList.remove("hitting");
    defender.classList.remove("defending");
  }, 200);
}

function onKeyUp(e, isPressedKeys) {
  // delete set key and delete state 'block' to fighter if needed
  switch (e.code) {
    case controls.PlayerOneAttack:
      isPressedKeys.PlayerOneAttack = 0;
      break;
    case controls.PlayerOneBlock:
      isPressedKeys.PlayerOneBlock = 0;
      document
        .querySelector(".arena___fighter.arena___left-fighter")
        .classList.remove("block");
      break;
    case controls.PlayerTwoAttack:
      isPressedKeys.PlayerTwoAttack = 0;
      break;
    case controls.PlayerTwoBlock:
      isPressedKeys.PlayerTwoBlock = 0;
      document
        .querySelector(".arena___fighter.arena___right-fighter")
        .classList.remove("block");
      break;
    case controls.PlayerOneCriticalHitCombination[0]:
      isPressedKeys.PlayerOneCriticalHitCombination[
        controls.PlayerOneCriticalHitCombination[0]
      ] = 0;
      break;
    case controls.PlayerOneCriticalHitCombination[1]:
      isPressedKeys.PlayerOneCriticalHitCombination[
        controls.PlayerOneCriticalHitCombination[1]
      ] = 0;
      break;
    case controls.PlayerOneCriticalHitCombination[2]:
      isPressedKeys.PlayerOneCriticalHitCombination[
        controls.PlayerOneCriticalHitCombination[2]
      ] = 0;
      break;
    case controls.PlayerTwoCriticalHitCombination[0]:
      isPressedKeys.PlayerTwoCriticalHitCombination[
        controls.PlayerTwoCriticalHitCombination[0]
      ] = 0;
      break;
    case controls.PlayerTwoCriticalHitCombination[1]:
      isPressedKeys.PlayerTwoCriticalHitCombination[
        controls.PlayerTwoCriticalHitCombination[1]
      ] = 0;
      break;
    case controls.PlayerTwoCriticalHitCombination[2]:
      isPressedKeys.PlayerTwoCriticalHitCombination[
        controls.PlayerTwoCriticalHitCombination[2]
      ] = 0;
      break;
    default:
      break;
  }
}

export function getDamage(attacker, defender) {
  if (attacker.isCritical) {
    return getHitPower(attacker);
  }
  const damage = getHitPower(attacker) - getBlockPower(defender);

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  if (fighter.isCritical) {
    fighter.isCritical = false;
    return fighter.power * 2;
  }
  const criticalHitChance = Math.random() + 1;

  return fighter.power * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;

  return fighter.defense * dodgeChance;
}
