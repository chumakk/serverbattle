const { fighter } = require("../models/fighter");

const createFighterValid = (req, res, next) => {
  try {
    isExist(req.body);
    allRequiredField(req.body);
    hasIdField(req.body);
    hasAnotherFields(req.body);
    powerValidator(req.body.power);
    defenseValidator(req.body.defense);
    if (!req.body.health) {
      req.body.health = 100;
    }
    healthValidator(req.body.health);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateFighterValid = (req, res, next) => {
  try {
    isExist(req.body);
    hasOneModelField(req.body);
    hasIdField(req.body);
    hasAnotherFields(req.body);
    if (req.body.power) {
      powerValidator(req.body.power);
    }
    if (req.body.defense) {
      defenseValidator(req.body.defense);
    }
    if (req.body.health) {
      healthValidator(req.body.health);
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

const isExist = (obj) => {
  if (!obj) throw Error("Fighter doesn't exist");
};

const allRequiredField = (obj) => {
  for (key in fighter) {
    if (key === "id") continue;
    if (key === "health") continue;
    if (!obj.hasOwnProperty(key)) {
      throw Error("Fighter hasn't all required fields");
    }
  }
};

const hasIdField = (obj) => {
  if (obj.hasOwnProperty("id"))
    throw Error("Fighter should haven't 'id' field");
};

const hasAnotherFields = (obj) => {
  for (key in obj) {
    if (!fighter.hasOwnProperty(key)) {
      throw Error("Fighter should haven't other fields");
    }
  }
};

const hasOneModelField = (obj) => {
  for (key in fighter) {
    if (key === "id") continue;
    if (obj.hasOwnProperty(key)) {
      return;
    }
  }
  throw Error("Fighter must have 1 field at least to update");
};

const powerValidator = (power) => {
  if (!isNaN(power) && parseFloat(power)) {
    if (!(+power > 1 && +power < 100)) {
      throw Error("Power should be in range (1-100)");
    }
  } else {
    throw Error("Power should be a number");
  }
};

const defenseValidator = (defense) => {
  if (!isNaN(defense) && parseFloat(defense)) {
    if (!(+defense > 1 && +defense < 10)) {
      throw Error("Defense should be in range (1-10)");
    }
  } else {
    throw Error("Defense should be a number");
  }
};

const healthValidator = (health) => {
  if (!isNaN(health) && parseFloat(health)) {
    if (!(+health > 80 && +health < 120)) {
      throw Error("Health should be in range (80-120)");
    }
  } else {
    throw Error("Health should be a number");
  }
};
