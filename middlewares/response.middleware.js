const responseMiddleware = (req, res, next) => {
  if (res.err) {
    if (res.err.message.includes("not found")) {
      res.status(404).json({ error: true, message: res.err.message });
    } else {
      res.status(400).json({ error: true, message: res.err.message });
    }
  } else {
    res.status(200).json(res.data);
  }
  next();
};

exports.responseMiddleware = responseMiddleware;
