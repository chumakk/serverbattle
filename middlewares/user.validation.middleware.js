const { isEmpty } = require("lodash");
const { user } = require("../models/user");

const createUserValid = (req, res, next) => {
  try {
    isExist(req.body);
    allRequiredField(req.body);
    hasIdField(req.body);
    hasAnotherFields(req.body);
    isGmailEmail(req.body.email);
    isUkrainianNumber(req.body.phoneNumber);
    isPasswordValid(req.body.password);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  try {
    isExist(req.body);
    hasOneModelField(req.body);
    hasIdField(req.body);
    hasAnotherFields(req.body);
    if (req.body.email) {
      isGmailEmail(req.body.email);
    }
    if (req.body.phoneNumber) {
      isUkrainianNumber(req.body.phoneNumber);
    }
    if (req.body.password) {
      isPasswordValid(req.body.password);
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

const isExist = (obj) => {
  if (!obj) throw Error("User doesn't exist");
};

const allRequiredField = (obj) => {
  for (key in user) {
    if (key == "id") continue;
    if (!obj.hasOwnProperty(key)) {
      throw Error("User hasn't all required fields");
    }
  }
};

const hasIdField = (obj) => {
  if (obj.hasOwnProperty("id")) throw Error("User should haven't 'id' field");
};

const hasAnotherFields = (obj) => {
  for (key in obj) {
    if (!user.hasOwnProperty(key)) {
      throw Error("User should haven't other fields");
    }
  }
};
const isGmailEmail = (email) => {
  const gmail = /^[a-zA-Z0-9]+[a-zA-Z0-9.]+@gmail.com$/;
  if (!gmail.test(email.toLowerCase())) throw Error("Email isn't valid");
};

const isUkrainianNumber = (phoneNumber) => {
  const ukrainian = /^\+380+\d{9}$/;
  if (!ukrainian.test(phoneNumber)) throw Error("Phone number isn't valid");
};

const isPasswordValid = (password) => {
  if (password.length < 3) throw Error("Password must have 3 symbols at least");
};

const hasOneModelField = (obj) => {
  for (key in user) {
    if (key == "id") continue;
    if (obj.hasOwnProperty(key)) {
      return;
    }
  }
  throw Error("User must have 1 field at least to update");
};
