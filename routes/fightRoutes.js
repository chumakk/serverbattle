const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

router
  .route("/")
  .get((req, res, next) => {
    try {
      const data = FightService.getAll();
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .post(
    (req, res, next) => {
      if (res.err) next();
      else {
        try {
          const data = FightService.createFight(req.body);
          res.data = data;
        } catch (err) {
          res.err = err;
        } finally {
          next();
        }
      }
    },
    responseMiddleware
  );

router
  .route("/:id")
  .get((req, res, next) => {
    try {
      const data = FightService.getFightById(req.params.id);
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .delete((req, res, next) => {
    try {
      const data = FightService.deleteFight(req.params.id);
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

module.exports = router;