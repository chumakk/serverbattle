const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

router
  .route("/")
  .get((req, res, next) => {
    try {
      const data = FighterService.getAll();
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .post(
    createFighterValid,
    (req, res, next) => {
      if (res.err) next();
      else {
        try {
          const data = FighterService.createFighter(req.body);
          res.data = data;
        } catch (err) {
          res.err = err;
        } finally {
          next();
        }
      }
    },
    responseMiddleware
  );

router
  .route("/:id")
  .get((req, res, next) => {
    try {
      const data = FighterService.getFighterById(req.params.id);
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .put(
    updateFighterValid,
    (req, res, next) => {
      if (res.err) next();
      else {
        try {
          const data = FighterService.fighterUpdate(req.params.id, req.body);
          res.data = data;
        } catch (err) {
          res.err = err;
        } finally {
          next();
        }
      }
    },
    responseMiddleware
  )
  .delete((req, res, next) => {
    try {
      const data = FighterService.deleteFighter(req.params.id);
      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

module.exports = router;
