const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router
  .route("/")
  .get((req, res, next) => {
    try {
      const users = UserService.getAll();
      res.data = users;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .post(
    createUserValid,
    (req, res, next) => {
      if (res.err) next();
      else {
        try {
          const createdUser = UserService.createUser(req.body);
          res.data = createdUser;
        } catch (err) {
          res.err = err;
        } finally {
          next();
        }
      }
    },
    responseMiddleware
  );

router
  .route("/:id")
  .get((req, res, next) => {
    try {
      const user = UserService.getUserById(req.params.id);
      res.data = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .put(
    updateUserValid,
    (req, res, next) => {
      if (res.err) next();
      else {
        try {
          const user = UserService.userUpdate(req.params.id, req.body);
          res.data = user;
        } catch (err) {
          res.err = err;
        } finally {
          next();
        }
      }
    },
    responseMiddleware
  )
  .delete((req, res, next) => {
    try {
      const user = UserService.deleteUser(req.params.id);
      res.data = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

module.exports = router;
