const { FightRepository } = require("../repositories/fightRepository");

class FightersService {
  createFight(fighter) {
    return FightRepository.create(fighter);
  }
  search(search) {
    const item = FightRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
  getFightById(id) {
    const fighter = this.search({ id });
    if (!fighter) {
      throw Error("Fight not found");
    }
    return fighter;
  }
  deleteFight(id) {
    return FightRepository.delete(id);
  }
  getAll() {
    return FightRepository.getAll();
  }
}

module.exports = new FightersService();
