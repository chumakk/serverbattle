const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  createFighter(fighter) {
    try {
      this._checkForEqualFighter(fighter);
      return FighterRepository.create(fighter);
    } catch (error) {
      throw Error(error.message);
    }
  }
  _checkForEqualFighter(fighter) {
    this._equalName(fighter);
  }

  _equalName({ name }) {
    if (!name) return;
    const isFighterExist = this.getAll().some((fighter) => {
      return fighter.name.toUpperCase() === name.toUpperCase();
    });
    if (isFighterExist) {
      throw Error("Fighter with this name exist");
    }
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getAll() {
    return FighterRepository.getAll();
  }

  getFighterById(id) {
    const fighter = this.search({ id });
    if (!fighter) {
      throw Error("Fighter not found");
    }
    return fighter;
  }

  fighterUpdate(id, data) {
    try {
      this.getFighterById(id);
    } catch (e) {
      throw Error("This fighter does not exist");
    }
    this._checkForEqualFighter(data);
    return FighterRepository.update(id, data);
  }

  deleteFighter(id) {
    return FighterRepository.delete(id);
  }
}

module.exports = new FighterService();
