const { UserRepository } = require("../repositories/userRepository");

class UserService {
  createUser(user) {
    try {
      this.prepareUser(user);
      this._checkForEqualUser(user);
      return UserRepository.create(user);
    } catch (error) {
      throw Error(error.message);
    }
  }

  prepareUser(user) {
    this._prepareEmail(user);
  }

  _prepareEmail(user) {
    if (user.email) {
      user.email = user.email.toLowerCase();
    }
  }

  _checkForEqualUser(user) {
    this._equalPhone(user);
    this._equalEmail(user);
  }

  _equalPhone({ phoneNumber }) {
    if (!phoneNumber) return;
    const user = this.search({ phoneNumber });
    if (user) {
      throw Error("User with this phone number exist");
    }
  }

  _equalEmail({ email }) {
    if (!email) return;
    const isUserExist = this.search({ email });
    if (isUserExist) {
      throw Error("User with this email exist");
    }
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getAll() {
    return UserRepository.getAll();
  }

  getUserById(id) {
    const user = this.search({ id });
    if (!user) {
      throw Error("User not found");
    }
    return user;
  }

  userUpdate(id, data) {
    try {
      this.getUserById(id);
    } catch (e) {
      throw Error("This user does not exist");
    }
    this.prepareUser(data);
    this._checkForEqualUser(data);
    return UserRepository.update(id, data);
  }

  deleteUser(id) {
    return UserRepository.delete(id);
  }
}

module.exports = new UserService();
